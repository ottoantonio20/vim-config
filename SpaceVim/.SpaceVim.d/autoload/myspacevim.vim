function! myspacevim#before() abort
  " Visual-Multi Config
  let g:VM_maps = {}
  let g:VM_maps['Find Under']         = '<C-d>'           " replace C-n
  let g:VM_maps['Find Subword Under'] = '<C-d>'           " replace visual C-n
  let g:VM_maps["Add Cursor Down"] = ''
  let g:VM_maps["Add Cursor Up"]   = ''
  let g:VM_mouse_mappings = 1
  nmap   <C-LeftMouse>         <Plug>(VM-Mouse-Cursor)
  nmap   <C-RightMouse>        <Plug>(VM-Mouse-Word)
  nmap   <M-C-RightMouse>      <Plug>(VM-Mouse-Column)
  " Set Config Clipboard external
  set clipboard=unnamedplus
  " Vim Config
  set updatetime=300
  " Coc Config
  let g:coc_global_extensions = [ 'coc-json', 'coc-snippets', 'coc-sh', 'coc-prettier', 'coc-phpls', 'coc-html-css-support', 'coc-html', 'coc-eslint', 'coc-emmet', 'coc-cssmodules', 'coc-vetur', 'coc-tsserver', 'coc-pyright', 'coc-css', 'coc-angular' ]
  " Startify Config
  let g:startify_session_autoload = 1
  let g:startify_session_persistence = 0
endfunction

function! myspacevim#after() abort
  " Vim Config
  set list
  set listchars=tab:→\ ,eol:↵,nbsp:␣,trail:·,extends:↷,precedes:↶,space:·
  " indentLine Config
  let g:indentLine_enabled = 1
  let g:indentLine_char = '|'
  " let g:indentLine_bgcolor_term = 202
  " let g:indentLine_bgcolor_gui = '#000'
  let g:indentLine_concealcursor = 'inc'
  let g:indentLine_conceallevel = 2
  " Better Whitespace Config
  let g:better_whitespace_enabled = 1
  let g:strip_whitespace_on_save = 0
  " Fix UltiSnips Error
  " let g:UltiSnipsUsePythonVersion = 3
  " call deoplete#custom#option('auto_complete_delay', 500)
  " Config Format Prettier
  " let g:neoformat_enabled_javascript = ['npxprettier']
endfunction
